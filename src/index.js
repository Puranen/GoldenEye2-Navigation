import React from 'react';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    Button
} from 'reactstrap';
import './style.css';

const defaultNavItemData = {
    'funds': {
        name: 'Funds',
        path: '/funds/'
    },
    'rp': {
        name: 'Rebalance strategies',
        path: '/rp/'
    },
    'rollDateAlerts': {
        name: 'Roll date alerts',
        path: '/rolldatealerts/'
    },
    'riskManagementReports': {
        name: 'Risk management reports',
        path: '/riskmanagementreports/'
    },
    'sisuReport': {
        name: 'SISU report',
        path: '/sisureport/'
    },
    'axReconciliation': {
        name: 'AxReconciliation',
        path: '/axreconciliation/'
    }

};

const activeStyle = {
    'backgroundColor':'#1d4c77',
    'fontWeight':'bold'
}

const normalStyle = {
    'backgroundColor':'transparent',
}

export default class Navigation extends React.Component {

    constructor(props) {
        super(props);
    }

    getNavItems(goldenEye2AppClaims, activeApp, basePath) {
        const navItems = goldenEye2AppClaims.map((claim) => {
            var itemData = defaultNavItemData[claim];
            if (itemData != null) {
                var isActive = activeApp == claim;
                return (
                    <NavItem id={"GE2NavItem" + itemData.name}>
                        <NavLink className="NAVitem" style={isActive ? activeStyle : normalStyle} href={basePath + itemData.path} active={isActive}>{itemData.name}</NavLink>
                    </NavItem>
                );
            }
        });
        return navItems;
    }

    render() {
        let {activeApp, basePath, goldenEye2AppClaims, sub} = this.props;
        var navItems = this.getNavItems(goldenEye2AppClaims, activeApp, basePath);

        return (
            <div>
                <Navbar className="mr-auto NAVcontent navbar-expand-lg" dark>
                    <NavbarBrand href="#" style={{marginLeft:15,fontWeight:'bold'}}>GoldenEye 2</NavbarBrand>
                    <Nav pills navbar style={{margin:'auto', marginLeft:0}}>
                        <NavItem id="GE2NavItemHome">
                            <NavLink className="NAVitem" href={basePath} active={activeApp == 'home'}>Home</NavLink>
                        </NavItem>
                        {navItems}
                    </Nav>
                    <span className="navbar-text login-state NAVuser">Current user: {sub}</span>
                </Navbar>
            </div>
        );
    }
}

Navigation.defaultProps = {
    activeApp: 'home',
    basePath: '/',
    rpClaim: false,
    fundsClaim: false,
    rollDateAlertsClaim: false,
    riskManagementReportsClaim: false,
    sisuReport: false,
    axReconciliation: false,
    currentUser: ''
}
